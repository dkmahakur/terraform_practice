
# Resource Section <-- Create our instance 

resource "aws_instance" "ec2" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    count = "${var.count}"
    associate_public_ip_address = "${var.ip_address}"

    root_block_device {
     volume_size = "${var.volume_size}"   
    }

    tags {
        Name = "${var.name}-${count.index+1}-${var.environment}-${var.project}"  # name-project-environment
        Region = "${var.region}"
        Count = "${var.count}"
        Timestamp = "${replace("${timestamp()}",":","-")}"
        Project = "${var.project}"
        Environment = "${var.environment}"
    }

}

resource "aws_ebs_volume" "new_volume1" {
    availability_zone = "${var.region}a"
    size = 20

    tags {
        Name = "${var.name}-${count.index+1}-${var.environment}-${var.project}-1"
    }

    depends_on = ["aws_instance.ec2"]

}

resource "aws_ebs_volume" "new_volume2" {
    availability_zone = "${var.region}a"
    size = 20

    tags {
        Name = "${var.name}-${count.index+1}-${var.environment}-${var.project}-2"
    }

    depends_on = ["aws_instance.ec2"]

}


resource "aws_volume_attachment" "new_attatch1" {
    device_name = "/dev/sdh"
    volume_id = "${aws_ebs_volume.new_volume1.id}"
    instance_id = "${aws_instance.ec2.id}"
}

resource "aws_volume_attachment" "new_attatch2" {
    device_name = "/dev/sdi"
    volume_id = "${aws_ebs_volume.new_volume2.id}"
    instance_id = "${aws_instance.ec2.id}"
}

# Output Section <-- Print results to the screen 

output "region" { value = "${var.region}" }
output "ami" { value = "${aws_instance.ec2.*.ami}" }
output "instance_type" { value = "${aws_instance.ec2.*.instance_type}" }



