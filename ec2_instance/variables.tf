# Variable Section <-- move this later 

variable "ami" { default = "ami-0e12cbde3e77cbb98"}
variable "instance_type" { default = "t2.micro"}
variable "count" { default = "3"}
variable "ip_address" { default = "true"}
variable "volume_size" { default = "35"}
variable "name" { default = "dibya"}
variable "project" { default = "terraform"}
variable "environment" { default = "dev"}
variable "region" { default = "eu-west-1"}