# Variable Section <-- move this later 

variable "ami" { default = "ami-0b0a60c0a2bd40612"}
variable "instance_type" { default = "t2.micro"}
variable "count" { default = "1"}
variable "ip_address" { default = "true"}
variable "volume_size" { default = "35"}
variable "name" { default = "dibya"}
variable "project" { default = "terraform"}
variable "environment" { default = "dev"}
variable "region" { default = "eu-central-1"}