resource "aws_s3_bucket" "log_bucket" {
  bucket = "dibya-terraform-log-buck"    # Make this unique
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "dibya-test2-bucket"         # Make this unique
  acl    = "private"

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }
}
