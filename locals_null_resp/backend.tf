terraform {
    backend "s3" {
        encrypt = true
        bucket = "dibya-remote-state-s3"
        key = "terraform/terraform.tfstate"
        region = "eu-central-1"
        access_key = "ENTER_HERE"
        secret_key = "ENTER_HERE"
    }
}