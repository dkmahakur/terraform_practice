resource "aws_s3_bucket" "remote_state" {
    bucket = "dibya-remote-state-s3"

    versioning {
        enabled = true
    }

    tags {
        Name = "dibya-remote-state-s3"
    }
}