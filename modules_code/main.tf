module "instance" {
  source = "../../ec2_instance" # put this in git
  name = "module"
  project = "testproject"
  environment = "production"
  count = 5
  
}

module "autoscaling" {
  source = "../../asg"
  schedule = "boxing_day"
  
}
