provider "aws" {
  access_key = "my secret access key"
  secret_key = "my secret key"
  region = "eu-central-1"
}
