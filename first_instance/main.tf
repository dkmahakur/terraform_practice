
resource "aws_instance" "test_instance" {
  ami = "${var.ami_value}"
  instance_type="${var.instance}"
  tags={
      Name="MyfirstInstance"
  }
  }
output "instance_ami" {
  value = "aws_instance.test_instance.ami"
}
output "instance_type" {
  value = "${aws_instance.test_instance.instance_type}"
}
